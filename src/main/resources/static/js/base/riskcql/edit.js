/**
 * 编辑-规则CQLjs
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		riskCql: {
			code: 0
		}
	},
	methods : {
		setForm: function() {
			$.SetForm({
				url: '../../risk/riskcql/info?_' + $.now(),
		    	param: vm.riskCql.code,
		    	success: function(data) {
		    		vm.riskCql = data;
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.ConfirmForm({
		    	url: '../../risk/riskcql/update?_' + $.now(),
		    	param: vm.riskCql,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})