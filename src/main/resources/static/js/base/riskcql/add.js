/**
 * 新增-规则CQLjs
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		riskCql: {
			code: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: '../../risk/riskcql/save?_' + $.now(),
		    	param: vm.riskCql,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
