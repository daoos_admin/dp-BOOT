/**
 * 新增-规则参数配置js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		ruleParameter: {
			code: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: '../../risk/ruleparameter/save?_' + $.now(),
		    	param: vm.ruleParameter,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
