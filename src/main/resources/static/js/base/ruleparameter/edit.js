/**
 * 编辑-规则参数配置js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		ruleParameter: {
			code: 0
		}
	},
	methods : {
		setForm: function() {
			$.SetForm({
				url: '../../risk/ruleparameter/info?_' + $.now(),
		    	param: vm.ruleParameter.code,
		    	success: function(data) {
		    		vm.ruleParameter = data;
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.ConfirmForm({
		    	url: '../../risk/ruleparameter/update?_' + $.now(),
		    	param: vm.ruleParameter,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})