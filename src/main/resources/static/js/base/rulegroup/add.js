/**
 * 新增-规则组js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		ruleGroup: {
			code: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: '../../risk/rulegroup/save?_' + $.now(),
		    	param: vm.ruleGroup,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
