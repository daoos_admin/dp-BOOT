/**
 * 编辑-规则组js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		ruleGroup: {
			code: 0
		}
	},
	methods : {
		setForm: function() {
			$.SetForm({
				url: '../../risk/rulegroup/info?_' + $.now(),
		    	param: vm.ruleGroup.code,
		    	success: function(data) {
		    		vm.ruleGroup = data;
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.ConfirmForm({
		    	url: '../../risk/rulegroup/update?_' + $.now(),
		    	param: vm.ruleGroup,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})