/**
 * 编辑-风控规则js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		riskRule: {
			code: 0
		}
	},
	methods : {
		setForm: function() {
			$.SetForm({
				url: '../../risk/riskrule/info?_' + $.now(),
		    	param: vm.riskRule.code,
		    	success: function(data) {
		    		vm.riskRule = data;
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.ConfirmForm({
		    	url: '../../risk/riskrule/update?_' + $.now(),
		    	param: vm.riskRule,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})