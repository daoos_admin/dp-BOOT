/**
 * 新增-风控规则js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		riskRule: {
			code: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: '../../risk/riskrule/save?_' + $.now(),
		    	param: vm.riskRule,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
