package com.daoos.modules.quartz.dao;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.modules.quartz.entity.QuartzJobEntity;
import com.daoos.modules.sys.dao.BaseMapper;


/**
 * 定时任务
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月20日 下午11:19:55
 */
@Mapper
public interface QuartzJobMapper extends BaseMapper<QuartzJobEntity> {

}
