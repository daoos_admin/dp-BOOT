package com.daoos.modules.quartz.dao;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.modules.quartz.entity.QuartzJobLogEntity;
import com.daoos.modules.sys.dao.BaseMapper;

/**
 * 定时任务日志
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月20日 下午11:04:51
 */
@Mapper
public interface QuartzJobLogMapper extends BaseMapper<QuartzJobLogEntity> {

	int batchRemoveAll();
	
}
