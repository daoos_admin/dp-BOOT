package com.daoos.modules.generator.service;

import java.util.Map;

import com.daoos.common.entity.Page;
import com.daoos.modules.generator.entity.GeneratorParamEntity;
import com.daoos.modules.generator.entity.TableEntity;

/**
 * 代码生成器
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月28日 下午8:55:29
 */
public interface SysGeneratorService {

	Page<TableEntity> listTable(Map<String, Object> params);
	
	byte[] generator(GeneratorParamEntity params);
	
}
