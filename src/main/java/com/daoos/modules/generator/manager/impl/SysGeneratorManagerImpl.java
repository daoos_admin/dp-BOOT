package com.daoos.modules.generator.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.modules.generator.dao.SysGeneratorMapper;
import com.daoos.modules.generator.entity.ColumnEntity;
import com.daoos.modules.generator.entity.TableEntity;
import com.daoos.modules.generator.manager.SysGeneratorManager;

/**
 * 代码生成器
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月28日 下午8:54:44
 */
@Component("sysGeneratorManager")
public class SysGeneratorManagerImpl implements SysGeneratorManager {

	@Autowired
	private SysGeneratorMapper sysGeneratorMapper;
	
	@Override
	public void listTable(Page<TableEntity> page, Query query) {
		sysGeneratorMapper.listTable(page, query);
	}

	@Override
	public TableEntity getTableByName(String tableName) {
		return sysGeneratorMapper.getTableByName(tableName);
	}

	@Override
	public List<ColumnEntity> listColumn(String tableName) {
		return sysGeneratorMapper.listColumn(tableName);
	}

}
