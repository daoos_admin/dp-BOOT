package com.daoos.modules.generator.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.modules.generator.entity.ColumnEntity;
import com.daoos.modules.generator.entity.TableEntity;

/**
 * 代码生成器
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月28日 下午8:47:12
 */
@Mapper
public interface SysGeneratorMapper {

	List<TableEntity> listTable(Page<TableEntity> page, Query query);
	
	TableEntity getTableByName(String tableName);
	
	List<ColumnEntity> listColumn(String tableName);
	
}
