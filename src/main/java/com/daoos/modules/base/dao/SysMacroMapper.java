package com.daoos.modules.base.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.modules.base.entity.SysMacroEntity;
import com.daoos.modules.sys.dao.BaseMapper;


/**
 * 通用字典
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月15日 下午12:46:31
 */
@Mapper
public interface SysMacroMapper extends BaseMapper<SysMacroEntity> {

	List<SysMacroEntity> listNotMacro();
	
	int countMacroChildren(Long typeId);
	
}
