package com.daoos.modules.base.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.common.entity.Query;
import com.daoos.modules.base.entity.SysAreaEntity;
import com.daoos.modules.sys.dao.BaseMapper;

/**
 * 行政区域
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月18日 下午3:36:04
 */
@Mapper
public interface SysAreaMapper extends BaseMapper<SysAreaEntity> {

	List<SysAreaEntity> listAreaByParentCode(Query query);
	
	int countAreaChildren(Long areaId);
	
}
