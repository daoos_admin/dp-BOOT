package com.daoos.modules.sys.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.daoos.common.utils.ShiroUtils;
import com.daoos.modules.sys.entity.SysUserEntity;

/**
 * Controller公共组件
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月8日 下午2:43:23
 */
public abstract class AbstractController {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected SysUserEntity getUser() {
		return ShiroUtils.getUserEntity();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}
	
}
