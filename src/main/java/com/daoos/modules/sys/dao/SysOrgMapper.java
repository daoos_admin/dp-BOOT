package com.daoos.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.modules.sys.entity.SysOrgEntity;

/**
 * 组织架构
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月17日 上午11:26:05
 */
@Mapper
public interface SysOrgMapper extends BaseMapper<SysOrgEntity> {

	int countOrgChildren(Long parentId);
	
}
