package com.daoos.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.modules.sys.entity.SysMenuEntity;

/**
 * 系统菜单dao
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月10日 上午12:21:34
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {
	
	List<SysMenuEntity> listParentId(Long parentId);
	
	List<SysMenuEntity> listNotButton();
	
	List<String> listUserPerms(Long userId);
	
	int countMenuChildren(Long parentId);

}
