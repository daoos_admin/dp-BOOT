package com.daoos.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.modules.sys.entity.SysRoleEntity;

/**
 * 系统角色
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月12日 上午12:35:51
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {
	
	List<String> listUserRoles(Long userId);
	
}
