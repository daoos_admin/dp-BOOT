package com.daoos.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.common.entity.Query;
import com.daoos.modules.sys.entity.SysUserEntity;

/**
 * 系统用户dao
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月8日 下午3:26:05
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

	SysUserEntity getByUserName(String username);
	
	List<Long> listAllMenuId(Long userId);
	
	List<Long> listAllOrgId(Long userId);
	
	int updatePswdByUser(Query query);
	
	int updateUserStatus(Query query);
	
	int updatePswd(SysUserEntity user);
	
}
