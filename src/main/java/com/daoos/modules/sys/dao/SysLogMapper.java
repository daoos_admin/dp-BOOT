package com.daoos.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.modules.sys.entity.SysLogEntity;

/**
 * 系统日志 
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月14日 下午8:19:01
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

	int batchRemoveAll();
	
}
