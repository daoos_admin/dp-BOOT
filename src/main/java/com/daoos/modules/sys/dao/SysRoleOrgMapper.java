package com.daoos.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.daoos.modules.sys.entity.SysRoleOrgEntity;

/**
 * 角色与机构的关系
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月17日 上午11:29:43
 */
@Mapper
public interface SysRoleOrgMapper extends BaseMapper<SysRoleOrgEntity> {

	List<Long> listOrgId(Long roleId);
	
	int batchRemoveByOrgId(Long[] id);
	
	int batchRemoveByRoleId(Long[] id);
	
}
