package com.daoos.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.modules.sys.entity.SysRoleMenuEntity;

/**
 * 系统角色与菜单关系
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月13日 下午8:32:26
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {

	int batchRemoveByMenuId(Long[] id);
	
	int batchRemoveByRoleId(Long[] id);
	
	List<Long> listMenuId(Long id);
	
}
