package com.daoos.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.modules.sys.entity.SysUserTokenEntity;

/**
 * 用户token
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年9月3日 上午3:29:17
 */
@Mapper
public interface SysUserTokenMapper extends BaseMapper<SysUserTokenEntity> {

	SysUserTokenEntity getByToken(String token);
	
	SysUserTokenEntity getByUserId(Long userId);
	
}
