package com.daoos.modules.sys.manager;

import java.util.List;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.modules.sys.entity.SysLogEntity;

/**
 * 系统日志
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月14日 下午8:43:06
 */
public interface SysLogManager {

	void saveLog(SysLogEntity log);
	
	List<SysLogEntity> listLog(Page<SysLogEntity> page, Query query);
	
	int batchRemove(Long[] id);
	
	int batchRemoveAll();
	
}
