package com.daoos.modules.sys.manager;

import java.util.List;

import com.daoos.modules.sys.entity.SysOrgEntity;

/**
 * 组织机构
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月17日 上午11:31:59
 */
public interface SysOrgManager {

	List<SysOrgEntity> listOrg();
	
	int saveOrg(SysOrgEntity org);
	
	SysOrgEntity getOrg(Long orgId);
	
	int updateOrg(SysOrgEntity org);
	
	int bactchRemoveOrg(Long[] id);
	
	boolean hasChildren(Long[] id);
	
}
