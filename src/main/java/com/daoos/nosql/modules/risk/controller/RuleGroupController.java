package com.daoos.nosql.modules.risk.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daoos.common.annotation.SysLog;
import com.daoos.modules.sys.controller.AbstractController;
import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RuleGroupEntity;
import com.daoos.nosql.modules.risk.service.RuleGroupService;

/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
@RestController
@RequestMapping("/risk/rulegroup")
public class RuleGroupController extends AbstractController {
	
	@Autowired
	private RuleGroupService ruleGroupService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<RuleGroupEntity> list(@RequestBody Map<String, Object> params) {
		return ruleGroupService.listRuleGroup(params);
	}
		
	/**
	 * 新增
	 * @param ruleGroup
	 * @return
	 */
	@SysLog("新增规则组")
	@RequestMapping("/save")
	public R save(@RequestBody RuleGroupEntity ruleGroup) {
		return ruleGroupService.saveRuleGroup(ruleGroup);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody Long id) {
		return ruleGroupService.getRuleGroupById(id);
	}
	
	/**
	 * 修改
	 * @param ruleGroup
	 * @return
	 */
	@SysLog("修改规则组")
	@RequestMapping("/update")
	public R update(@RequestBody RuleGroupEntity ruleGroup) {
		return ruleGroupService.updateRuleGroup(ruleGroup);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除规则组")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return ruleGroupService.batchRemove(id);
	}
	
}
