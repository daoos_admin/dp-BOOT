package com.daoos.nosql.modules.risk.service;

import java.util.Map;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RiskCqlEntity;

/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
public interface RiskCqlService {

	Page<RiskCqlEntity> listRiskCql(Map<String, Object> params);
	
	R saveRiskCql(RiskCqlEntity riskCql);
	
	R getRiskCqlById(Long id);
	
	R updateRiskCql(RiskCqlEntity riskCql);
	
	R batchRemove(Long[] id);
	
}
