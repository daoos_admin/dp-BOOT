package com.daoos.nosql.modules.risk.manager;

import java.util.List;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.entity.RuleGroupEntity;

/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
public interface RuleGroupManager {

	List<RuleGroupEntity> listRuleGroup(Page<RuleGroupEntity> page, Query search);
	
	int saveRuleGroup(RuleGroupEntity ruleGroup);
	
	RuleGroupEntity getRuleGroupById(Long id);
	
	int updateRuleGroup(RuleGroupEntity ruleGroup);
	
	int batchRemove(Long[] id);
	
}
