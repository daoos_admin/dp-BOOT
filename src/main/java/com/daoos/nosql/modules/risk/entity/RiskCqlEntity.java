package com.daoos.nosql.modules.risk.entity;

import java.io.Serializable;


/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
public class RiskCqlEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 编号
	 */
	private String code;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 版本
	 */
	private String version;
	
	/**
	 * CQL脚本
	 */
	private String cql;
	

	public RiskCqlEntity() {
		super();
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setCql(String cql) {
		this.cql = cql;
	}
	
	public String getCql() {
		return cql;
	}
	
}
