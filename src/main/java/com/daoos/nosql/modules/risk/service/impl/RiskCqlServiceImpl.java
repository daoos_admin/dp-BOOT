package com.daoos.nosql.modules.risk.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.common.entity.R;
import com.daoos.common.utils.CommonUtils;
import com.daoos.nosql.modules.risk.entity.RiskCqlEntity;
import com.daoos.nosql.modules.risk.manager.RiskCqlManager;
import com.daoos.nosql.modules.risk.service.RiskCqlService;

/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
@Service("riskCqlService")
public class RiskCqlServiceImpl implements RiskCqlService {

	@Autowired
	private RiskCqlManager riskCqlManager;

	@Override
	public Page<RiskCqlEntity> listRiskCql(Map<String, Object> params) {
		Query query = new Query(params);
		Page<RiskCqlEntity> page = new Page<>(query);
		riskCqlManager.listRiskCql(page, query);
		return page;
	}

	@Override
	public R saveRiskCql(RiskCqlEntity role) {
		int count = riskCqlManager.saveRiskCql(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getRiskCqlById(Long id) {
		RiskCqlEntity riskCql = riskCqlManager.getRiskCqlById(id);
		return CommonUtils.msg(riskCql);
	}

	@Override
	public R updateRiskCql(RiskCqlEntity riskCql) {
		int count = riskCqlManager.updateRiskCql(riskCql);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = riskCqlManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

}
