package com.daoos.nosql.modules.risk.entity;

import java.io.Serializable;


/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
public class RuleGroupEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 规则组编号
	 */
	private String code;
	
	/**
	 * 规则组名
	 */
	private String name;
	

	public RuleGroupEntity() {
		super();
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}
