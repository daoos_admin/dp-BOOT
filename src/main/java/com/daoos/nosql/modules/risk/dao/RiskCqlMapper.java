package com.daoos.nosql.modules.risk.dao;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.nosql.modules.risk.entity.RiskCqlEntity;
import com.daoos.modules.sys.dao.BaseMapper;

/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
@Mapper
public interface RiskCqlMapper extends BaseMapper<RiskCqlEntity> {
	
}
