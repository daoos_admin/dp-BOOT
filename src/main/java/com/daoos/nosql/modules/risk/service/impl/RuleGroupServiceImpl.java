package com.daoos.nosql.modules.risk.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.common.entity.R;
import com.daoos.common.utils.CommonUtils;
import com.daoos.nosql.modules.risk.entity.RuleGroupEntity;
import com.daoos.nosql.modules.risk.manager.RuleGroupManager;
import com.daoos.nosql.modules.risk.service.RuleGroupService;

/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
@Service("ruleGroupService")
public class RuleGroupServiceImpl implements RuleGroupService {

	@Autowired
	private RuleGroupManager ruleGroupManager;

	@Override
	public Page<RuleGroupEntity> listRuleGroup(Map<String, Object> params) {
		Query query = new Query(params);
		Page<RuleGroupEntity> page = new Page<>(query);
		ruleGroupManager.listRuleGroup(page, query);
		return page;
	}

	@Override
	public R saveRuleGroup(RuleGroupEntity role) {
		int count = ruleGroupManager.saveRuleGroup(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getRuleGroupById(Long id) {
		RuleGroupEntity ruleGroup = ruleGroupManager.getRuleGroupById(id);
		return CommonUtils.msg(ruleGroup);
	}

	@Override
	public R updateRuleGroup(RuleGroupEntity ruleGroup) {
		int count = ruleGroupManager.updateRuleGroup(ruleGroup);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = ruleGroupManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

}
