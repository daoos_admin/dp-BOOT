package com.daoos.nosql.modules.risk.service;

import java.util.Map;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RiskRuleEntity;

/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
public interface RiskRuleService {

	Page<RiskRuleEntity> listRiskRule(Map<String, Object> params);
	
	R saveRiskRule(RiskRuleEntity riskRule);
	
	R getRiskRuleById(Long id);
	
	R updateRiskRule(RiskRuleEntity riskRule);
	
	R batchRemove(Long[] id);
	
}
