package com.daoos.nosql.modules.risk.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.dao.RiskCqlMapper;
import com.daoos.nosql.modules.risk.entity.RiskCqlEntity;
import com.daoos.nosql.modules.risk.manager.RiskCqlManager;

/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
@Component("riskCqlManager")
public class RiskCqlManagerImpl implements RiskCqlManager {

	@Autowired
	private RiskCqlMapper riskCqlMapper;
	

	@Override
	public List<RiskCqlEntity> listRiskCql(Page<RiskCqlEntity> page, Query search) {
		return riskCqlMapper.listForPage(page, search);
	}

	@Override
	public int saveRiskCql(RiskCqlEntity riskCql) {
		return riskCqlMapper.save(riskCql);
	}

	@Override
	public RiskCqlEntity getRiskCqlById(Long id) {
		RiskCqlEntity riskCql = riskCqlMapper.getObjectById(id);
		return riskCql;
	}

	@Override
	public int updateRiskCql(RiskCqlEntity riskCql) {
		return riskCqlMapper.update(riskCql);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = riskCqlMapper.batchRemove(id);
		return count;
	}
	
}
