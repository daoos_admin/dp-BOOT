package com.daoos.nosql.modules.risk.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.common.entity.R;
import com.daoos.common.utils.CommonUtils;
import com.daoos.nosql.modules.risk.entity.RuleParameterEntity;
import com.daoos.nosql.modules.risk.manager.RuleParameterManager;
import com.daoos.nosql.modules.risk.service.RuleParameterService;

/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
@Service("ruleParameterService")
public class RuleParameterServiceImpl implements RuleParameterService {

	@Autowired
	private RuleParameterManager ruleParameterManager;

	@Override
	public Page<RuleParameterEntity> listRuleParameter(Map<String, Object> params) {
		Query query = new Query(params);
		Page<RuleParameterEntity> page = new Page<>(query);
		ruleParameterManager.listRuleParameter(page, query);
		return page;
	}

	@Override
	public R saveRuleParameter(RuleParameterEntity role) {
		int count = ruleParameterManager.saveRuleParameter(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getRuleParameterById(Long id) {
		RuleParameterEntity ruleParameter = ruleParameterManager.getRuleParameterById(id);
		return CommonUtils.msg(ruleParameter);
	}

	@Override
	public R updateRuleParameter(RuleParameterEntity ruleParameter) {
		int count = ruleParameterManager.updateRuleParameter(ruleParameter);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = ruleParameterManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

}
