package com.daoos.nosql.modules.risk.entity;

import java.io.Serializable;


/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
public class RiskRuleEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 规则编号
	 */
	private Integer code;
	
	/**
	 * 规则名
	 */
	private String name;
	
	/**
	 * 规则说明
	 */
	private String note;
	
	/**
	 * 命中处理方式
	 */
	private Integer action;
	
	/**
	 * 处理方式
	 */
	private Integer type;
	
	/**
	 * 处理类
	 */
	private String className;
	
	/**
	 * 类处理方法
	 */
	private String methodName;
	

	public RiskRuleEntity() {
		super();
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setAction(Integer action) {
		this.action = action;
	}
	
	public Integer getAction() {
		return action;
	}
	
	public void setType(Integer type) {
		this.type = type;
	}
	
	public Integer getType() {
		return type;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public String getMethodName() {
		return methodName;
	}
	
}
