package com.daoos.nosql.modules.risk.service;

import java.util.Map;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RuleParameterEntity;

/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
public interface RuleParameterService {

	Page<RuleParameterEntity> listRuleParameter(Map<String, Object> params);
	
	R saveRuleParameter(RuleParameterEntity ruleParameter);
	
	R getRuleParameterById(Long id);
	
	R updateRuleParameter(RuleParameterEntity ruleParameter);
	
	R batchRemove(Long[] id);
	
}
