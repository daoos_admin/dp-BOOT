package com.daoos.nosql.modules.risk.manager;

import java.util.List;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.entity.RuleParameterEntity;

/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
public interface RuleParameterManager {

	List<RuleParameterEntity> listRuleParameter(Page<RuleParameterEntity> page, Query search);
	
	int saveRuleParameter(RuleParameterEntity ruleParameter);
	
	RuleParameterEntity getRuleParameterById(Long id);
	
	int updateRuleParameter(RuleParameterEntity ruleParameter);
	
	int batchRemove(Long[] id);
	
}
