package com.daoos.nosql.modules.risk.manager;

import java.util.List;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.entity.RiskCqlEntity;

/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
public interface RiskCqlManager {

	List<RiskCqlEntity> listRiskCql(Page<RiskCqlEntity> page, Query search);
	
	int saveRiskCql(RiskCqlEntity riskCql);
	
	RiskCqlEntity getRiskCqlById(Long id);
	
	int updateRiskCql(RiskCqlEntity riskCql);
	
	int batchRemove(Long[] id);
	
}
