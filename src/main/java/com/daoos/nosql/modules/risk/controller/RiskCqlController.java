package com.daoos.nosql.modules.risk.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daoos.common.annotation.SysLog;
import com.daoos.modules.sys.controller.AbstractController;
import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RiskCqlEntity;
import com.daoos.nosql.modules.risk.service.RiskCqlService;

/**
 * 规则CQL
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:20:35
 */
@RestController
@RequestMapping("/risk/riskcql")
public class RiskCqlController extends AbstractController {
	
	@Autowired
	private RiskCqlService riskCqlService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<RiskCqlEntity> list(@RequestBody Map<String, Object> params) {
		return riskCqlService.listRiskCql(params);
	}
		
	/**
	 * 新增
	 * @param riskCql
	 * @return
	 */
	@SysLog("新增规则CQL")
	@RequestMapping("/save")
	public R save(@RequestBody RiskCqlEntity riskCql) {
		return riskCqlService.saveRiskCql(riskCql);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody Long id) {
		return riskCqlService.getRiskCqlById(id);
	}
	
	/**
	 * 修改
	 * @param riskCql
	 * @return
	 */
	@SysLog("修改规则CQL")
	@RequestMapping("/update")
	public R update(@RequestBody RiskCqlEntity riskCql) {
		return riskCqlService.updateRiskCql(riskCql);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除规则CQL")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return riskCqlService.batchRemove(id);
	}
	
}
