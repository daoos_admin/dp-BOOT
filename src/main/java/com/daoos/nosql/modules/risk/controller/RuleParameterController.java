package com.daoos.nosql.modules.risk.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daoos.common.annotation.SysLog;
import com.daoos.modules.sys.controller.AbstractController;
import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RuleParameterEntity;
import com.daoos.nosql.modules.risk.service.RuleParameterService;

/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
@RestController
@RequestMapping("/risk/ruleparameter")
public class RuleParameterController extends AbstractController {
	
	@Autowired
	private RuleParameterService ruleParameterService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<RuleParameterEntity> list(@RequestBody Map<String, Object> params) {
		return ruleParameterService.listRuleParameter(params);
	}
		
	/**
	 * 新增
	 * @param ruleParameter
	 * @return
	 */
	@SysLog("新增规则参数配置")
	@RequestMapping("/save")
	public R save(@RequestBody RuleParameterEntity ruleParameter) {
		return ruleParameterService.saveRuleParameter(ruleParameter);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody Long id) {
		return ruleParameterService.getRuleParameterById(id);
	}
	
	/**
	 * 修改
	 * @param ruleParameter
	 * @return
	 */
	@SysLog("修改规则参数配置")
	@RequestMapping("/update")
	public R update(@RequestBody RuleParameterEntity ruleParameter) {
		return ruleParameterService.updateRuleParameter(ruleParameter);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除规则参数配置")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return ruleParameterService.batchRemove(id);
	}
	
}
