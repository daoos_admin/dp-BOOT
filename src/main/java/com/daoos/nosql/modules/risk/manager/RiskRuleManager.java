package com.daoos.nosql.modules.risk.manager;

import java.util.List;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.entity.RiskRuleEntity;

/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
public interface RiskRuleManager {

	List<RiskRuleEntity> listRiskRule(Page<RiskRuleEntity> page, Query search);
	
	int saveRiskRule(RiskRuleEntity riskRule);
	
	RiskRuleEntity getRiskRuleById(Long id);
	
	int updateRiskRule(RiskRuleEntity riskRule);
	
	int batchRemove(Long[] id);
	
}
