package com.daoos.nosql.modules.risk.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.dao.RuleParameterMapper;
import com.daoos.nosql.modules.risk.entity.RuleParameterEntity;
import com.daoos.nosql.modules.risk.manager.RuleParameterManager;

/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
@Component("ruleParameterManager")
public class RuleParameterManagerImpl implements RuleParameterManager {

	@Autowired
	private RuleParameterMapper ruleParameterMapper;
	

	@Override
	public List<RuleParameterEntity> listRuleParameter(Page<RuleParameterEntity> page, Query search) {
		return ruleParameterMapper.listForPage(page, search);
	}

	@Override
	public int saveRuleParameter(RuleParameterEntity ruleParameter) {
		return ruleParameterMapper.save(ruleParameter);
	}

	@Override
	public RuleParameterEntity getRuleParameterById(Long id) {
		RuleParameterEntity ruleParameter = ruleParameterMapper.getObjectById(id);
		return ruleParameter;
	}

	@Override
	public int updateRuleParameter(RuleParameterEntity ruleParameter) {
		return ruleParameterMapper.update(ruleParameter);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = ruleParameterMapper.batchRemove(id);
		return count;
	}
	
}
