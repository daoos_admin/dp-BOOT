package com.daoos.nosql.modules.risk.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.dao.RuleGroupMapper;
import com.daoos.nosql.modules.risk.entity.RuleGroupEntity;
import com.daoos.nosql.modules.risk.manager.RuleGroupManager;

/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
@Component("ruleGroupManager")
public class RuleGroupManagerImpl implements RuleGroupManager {

	@Autowired
	private RuleGroupMapper ruleGroupMapper;
	

	@Override
	public List<RuleGroupEntity> listRuleGroup(Page<RuleGroupEntity> page, Query search) {
		return ruleGroupMapper.listForPage(page, search);
	}

	@Override
	public int saveRuleGroup(RuleGroupEntity ruleGroup) {
		return ruleGroupMapper.save(ruleGroup);
	}

	@Override
	public RuleGroupEntity getRuleGroupById(Long id) {
		RuleGroupEntity ruleGroup = ruleGroupMapper.getObjectById(id);
		return ruleGroup;
	}

	@Override
	public int updateRuleGroup(RuleGroupEntity ruleGroup) {
		return ruleGroupMapper.update(ruleGroup);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = ruleGroupMapper.batchRemove(id);
		return count;
	}
	
}
