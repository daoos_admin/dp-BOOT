package com.daoos.nosql.modules.risk.dao;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.nosql.modules.risk.entity.RuleGroupEntity;
import com.daoos.modules.sys.dao.BaseMapper;

/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
@Mapper
public interface RuleGroupMapper extends BaseMapper<RuleGroupEntity> {
	
}
