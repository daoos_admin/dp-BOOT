package com.daoos.nosql.modules.risk.dao;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.nosql.modules.risk.entity.RuleParameterEntity;
import com.daoos.modules.sys.dao.BaseMapper;

/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
@Mapper
public interface RuleParameterMapper extends BaseMapper<RuleParameterEntity> {
	
}
