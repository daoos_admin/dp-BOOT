package com.daoos.nosql.modules.risk.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.common.entity.R;
import com.daoos.common.utils.CommonUtils;
import com.daoos.nosql.modules.risk.entity.RiskRuleEntity;
import com.daoos.nosql.modules.risk.manager.RiskRuleManager;
import com.daoos.nosql.modules.risk.service.RiskRuleService;

/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
@Service("riskRuleService")
public class RiskRuleServiceImpl implements RiskRuleService {

	@Autowired
	private RiskRuleManager riskRuleManager;

	@Override
	public Page<RiskRuleEntity> listRiskRule(Map<String, Object> params) {
		Query query = new Query(params);
		Page<RiskRuleEntity> page = new Page<>(query);
		riskRuleManager.listRiskRule(page, query);
		return page;
	}

	@Override
	public R saveRiskRule(RiskRuleEntity role) {
		int count = riskRuleManager.saveRiskRule(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getRiskRuleById(Long id) {
		RiskRuleEntity riskRule = riskRuleManager.getRiskRuleById(id);
		return CommonUtils.msg(riskRule);
	}

	@Override
	public R updateRiskRule(RiskRuleEntity riskRule) {
		int count = riskRuleManager.updateRiskRule(riskRule);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = riskRuleManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

}
