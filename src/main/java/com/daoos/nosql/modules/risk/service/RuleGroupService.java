package com.daoos.nosql.modules.risk.service;

import java.util.Map;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RuleGroupEntity;

/**
 * 规则组
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:16:46
 */
public interface RuleGroupService {

	Page<RuleGroupEntity> listRuleGroup(Map<String, Object> params);
	
	R saveRuleGroup(RuleGroupEntity ruleGroup);
	
	R getRuleGroupById(Long id);
	
	R updateRuleGroup(RuleGroupEntity ruleGroup);
	
	R batchRemove(Long[] id);
	
}
