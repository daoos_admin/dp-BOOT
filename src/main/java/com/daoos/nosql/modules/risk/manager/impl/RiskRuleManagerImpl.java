package com.daoos.nosql.modules.risk.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.daoos.common.entity.Page;
import com.daoos.common.entity.Query;
import com.daoos.nosql.modules.risk.dao.RiskRuleMapper;
import com.daoos.nosql.modules.risk.entity.RiskRuleEntity;
import com.daoos.nosql.modules.risk.manager.RiskRuleManager;

/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
@Component("riskRuleManager")
public class RiskRuleManagerImpl implements RiskRuleManager {

	@Autowired
	private RiskRuleMapper riskRuleMapper;
	

	@Override
	public List<RiskRuleEntity> listRiskRule(Page<RiskRuleEntity> page, Query search) {
		return riskRuleMapper.listForPage(page, search);
	}

	@Override
	public int saveRiskRule(RiskRuleEntity riskRule) {
		return riskRuleMapper.save(riskRule);
	}

	@Override
	public RiskRuleEntity getRiskRuleById(Long id) {
		RiskRuleEntity riskRule = riskRuleMapper.getObjectById(id);
		return riskRule;
	}

	@Override
	public int updateRiskRule(RiskRuleEntity riskRule) {
		return riskRuleMapper.update(riskRule);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = riskRuleMapper.batchRemove(id);
		return count;
	}
	
}
