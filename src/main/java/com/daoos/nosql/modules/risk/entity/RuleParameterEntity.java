package com.daoos.nosql.modules.risk.entity;

import java.io.Serializable;


/**
 * 规则参数配置
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM10:25:09
 */
public class RuleParameterEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 参数Key
	 */
	private String code;
	
	/**
	 * 参数名
	 */
	private String name;
	
	/**
	 * 参数描述
	 */
	private String note;
	
	/**
	 * 参数校验类型
	 */
	private String type;
	

	public RuleParameterEntity() {
		super();
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
}
