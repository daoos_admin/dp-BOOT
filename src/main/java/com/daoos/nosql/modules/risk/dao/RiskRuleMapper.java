package com.daoos.nosql.modules.risk.dao;

import org.apache.ibatis.annotations.Mapper;

import com.daoos.nosql.modules.risk.entity.RiskRuleEntity;
import com.daoos.modules.sys.dao.BaseMapper;

/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
@Mapper
public interface RiskRuleMapper extends BaseMapper<RiskRuleEntity> {
	
}
