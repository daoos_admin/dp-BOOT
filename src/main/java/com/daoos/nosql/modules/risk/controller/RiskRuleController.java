package com.daoos.nosql.modules.risk.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daoos.common.annotation.SysLog;
import com.daoos.modules.sys.controller.AbstractController;
import com.daoos.common.entity.Page;
import com.daoos.common.entity.R;
import com.daoos.nosql.modules.risk.entity.RiskRuleEntity;
import com.daoos.nosql.modules.risk.service.RiskRuleService;

/**
 * 风控规则
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2018年5月11日 AM9:52:33
 */
@RestController
@RequestMapping("/risk/riskrule")
public class RiskRuleController extends AbstractController {
	
	@Autowired
	private RiskRuleService riskRuleService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<RiskRuleEntity> list(@RequestBody Map<String, Object> params) {
		return riskRuleService.listRiskRule(params);
	}
		
	/**
	 * 新增
	 * @param riskRule
	 * @return
	 */
	@SysLog("新增风控规则")
	@RequestMapping("/save")
	public R save(@RequestBody RiskRuleEntity riskRule) {
		return riskRuleService.saveRiskRule(riskRule);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody Long id) {
		return riskRuleService.getRiskRuleById(id);
	}
	
	/**
	 * 修改
	 * @param riskRule
	 * @return
	 */
	@SysLog("修改风控规则")
	@RequestMapping("/update")
	public R update(@RequestBody RiskRuleEntity riskRule) {
		return riskRuleService.updateRiskRule(riskRule);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除风控规则")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return riskRuleService.batchRemove(id);
	}
	
}
