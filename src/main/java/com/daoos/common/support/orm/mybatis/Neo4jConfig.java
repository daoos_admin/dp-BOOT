package com.daoos.common.support.orm.mybatis;//package io.renren.config;

/**
 * Created by daoos on 18-4-24.
 */

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * Created by summer on 2016/11/25.
 */
@Configuration
@MapperScan(basePackages = {"com.daoos.nosql.modules.risk.dao"}, sqlSessionTemplateRef = "neo4jSqlSessionTemplate")
public class Neo4jConfig {

    @Bean(name = "neo4jDataSource")
    @ConfigurationProperties(prefix = "spring.neo4jdatasource")
    public DataSource testDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "neo4jSqlSessionFactory")
    public SqlSessionFactory neo4jSqlSessionFactory(@Qualifier("neo4jDataSource") DataSource neo4jDataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(neo4jDataSource);
        bean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource("classpath:neo4jmybatis.xml"));
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/nosql/**/*.xml"));
        return bean.getObject();
    }

    @Bean(name = "neo4jTransactionManager")
    public DataSourceTransactionManager neo4jTransactionManager(@Qualifier("neo4jDataSource") DataSource neo4jDataSource) {
        return new DataSourceTransactionManager(neo4jDataSource);
    }

    @Bean(name = "neo4jSqlSessionTemplate")
    public SqlSessionTemplate test1SqlSessionTemplate(@Qualifier("neo4jSqlSessionFactory") SqlSessionFactory neo4jSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(neo4jSqlSessionFactory);
    }

}