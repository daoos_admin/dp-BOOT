package com.daoos.common.support.orm.dialect;

import org.apache.ibatis.session.Configuration;

import java.util.HashMap;

/**
 * 数据库方言工厂,产生方言对象
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月8日 上午11:06:30
 */
public class DialectFactory {

    public static String dialectClass = null;
public static HashMap<String,Dialect> dialectHashMap=new HashMap<String,Dialect>();
    public static Dialect buildDialect(Configuration configuration) {
//        if (dialectClass == null) {
//            synchronized (DialectFactory.class) {
//                if (dialectClass == null) {
//                    dialectClass = configuration.getVariables().getProperty("dialectClass");
//                }
//            }
//        }
        String dialectClass = configuration.getVariables().getProperty("dialectClass");
        Dialect dialect = null;
        try {
            dialect = (Dialect) Class.forName(dialectClass).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("请检查 mybatis-config.xml 中  dialectClass 是否配置正确?");
        }
        return dialect;
    }
}
