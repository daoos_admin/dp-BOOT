package com.daoos.common.support.orm.dialect;


/**
 * MySql5PageHepler
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月8日 上午11:07:21
 */
public class Neo4jPageHepler {
    /**
     * 得到查询总数的sql
     */
    public static String getCountString(String querySelect) {

        querySelect = getLineSql(querySelect);
        int lastReturnIndex = getLastReturnInsertPoint(querySelect);
        StringBuilder sb = new StringBuilder(querySelect);
        sb.replace(lastReturnIndex,lastReturnIndex+8,"  with  ");
        return sb.append(" return count(*) as count").toString();
    }

    /**
     * 得到最后一个Order By的插入点位置
     * 
     * @return 返回最后一个Order By插入点的位置
     */
    private static int getLastReturnInsertPoint(String querySelect) {
        int orderIndex = querySelect.toLowerCase().lastIndexOf(" return ");
        if (orderIndex == -1) {
            orderIndex = querySelect.length();
        }
        return orderIndex;
    }

    /**
     * 得到分页的SQL
     * 
     * @param offset
     *            偏移量
     * @param limit
     *            位置
     * @return 分页SQL
     */
    public static String getLimitString(String querySelect, int offset, int limit) {

        querySelect = getLineSql(querySelect);

        String sql = querySelect + " skip " + offset + " limit " + limit;

        return sql;

    }

    /**
     * 将SQL语句变成一条语句，并且每个单词的间隔都是1个空格
     * 
     * @param sql
     *            SQL语句
     * @return 如果sql是NULL返回空，否则返回转化后的SQL
     */
    private static String getLineSql(String sql) {
        return sql.replaceAll("[\r\n]", " ").replaceAll("\\s{2,}", " ");
    }


}
