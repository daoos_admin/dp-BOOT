package com.daoos.common.support.orm.dialect;

/**
 * Postgre 数据库 方言
 *
 * @author daoos
 * @email daoos@daoos.com
 * @url www.daoos.com
 * @date 2017年8月8日 上午11:07:36
 */
public class PostgreDialect extends Dialect {

    protected static final String SQL_END_DELIMITER = ";";

    @Override
    public String getLimitString(String sql, int offset, int limit) {
        return PostgrePageHepler.getLimitString(sql, offset, limit);
    }

    @Override
    public String getCountString(String sql) {
        return PostgrePageHepler.getCountString(sql);
    }
}
